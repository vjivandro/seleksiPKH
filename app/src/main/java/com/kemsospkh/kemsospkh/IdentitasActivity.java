package com.kemsospkh.kemsospkh;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kemsospkh.kemsospkh.helper.DatabaseHelper;
import com.kemsospkh.kemsospkh.model.IdentitasModel;
import com.kemsospkh.kemsospkh.model.ProfilModel;

public class IdentitasActivity extends AppCompatActivity {

    TextView txt_nik, txt_alamtktp, txt_tempat, txt_tanggal, txt_jk, txt_kebangsaan, txt_merried, txt_agama;
    TextView txt_tambah, lihat_ktp;
    String nik, alamatktp, tempat_lahir, tanggal_lahir, jenis_kelamin,kebangsaan, merried, agama;
    public ImageView image_ktp;
    Dialog mDialog;
    Cursor cursor;

    public static final String TAG_NAMA = "nama";
    public static final String TAG_NIK = "nik";
    public static final String TAG_TEMPAT_LAHIR = "tempat_lahir";
    public static final String TAG_TANGGAL_LAHIR = "tanggal_lahir";

    public static final String my_shared_preferences = "my_shared_preferences";

    SharedPreferences sharedPreferences;
    public DatabaseHelper db = new DatabaseHelper((this));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ktp);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txt_nik = (TextView) findViewById(R.id.tv_nik);
        txt_alamtktp = (TextView) findViewById(R.id.tv_almKtp);
        txt_tempat = (TextView) findViewById(R.id.tv_tempat);
        txt_tanggal = (TextView) findViewById(R.id.tv_tanggal);
        txt_jk = (TextView) findViewById(R.id.tv_jeniskelamin);
        txt_kebangsaan = (TextView) findViewById(R.id.tv_kebangsaan);
        txt_merried = (TextView) findViewById(R.id.tv_statuskawin);
        txt_agama = (TextView) findViewById(R.id.tv_agama);
        lihat_ktp = (TextView) findViewById(R.id.tv_fotoKtp);


        sharedPreferences = getSharedPreferences(LoginActivity.my_shared_preferences, Context.MODE_PRIVATE);

        nik = getIntent().getStringExtra(TAG_NIK);
        txt_nik.setText(nik);

        txt_tambah = (TextView) findViewById(R.id.tambah);
        txt_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent tambah = new Intent(IdentitasActivity.this, TambahIdentitasActivity.class);
                tambah.putExtra(TAG_NIK, nik);
                startActivity(tambah);
            }
        });

        lihat_ktp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IdentitasModel identitasModel = db.getDataIdentitas();
                mDialog = new Dialog(IdentitasActivity.this);
                mDialog.setContentView(R.layout.foto_dialog);
                image_ktp = (ImageView)mDialog.findViewById(R.id.imageView2);
                image_ktp.setImageBitmap(BitmapFactory.decodeByteArray(identitasModel.foto_ktp, 0, identitasModel.foto_ktp.length));
                mDialog.show();
            }
        });

        IdentitasModel identitasModel = db.getDataIdentitas();

        txt_nik.setText(nik);
        txt_alamtktp.setText("Alamat Sesuai Domisili");
        txt_tempat.setText(identitasModel.tempat_lahir);
        txt_tanggal.setText(identitasModel.tanggal_lahir);
        txt_kebangsaan.setText(identitasModel.kebangsaan);
        txt_jk.setText(identitasModel.jenis_kelamin);
        txt_merried.setText(identitasModel.merried);
        txt_agama.setText(identitasModel.agama);
    }


    public void Refresh(){
        DatabaseHelper db = new DatabaseHelper((this));
        IdentitasModel identitasModel = db.getDataIdentitas();

        txt_nik.setText(identitasModel.nik);
        txt_alamtktp.setText(identitasModel.alamatKtp);
        txt_tempat.setText(identitasModel.tempat_lahir);
        txt_tanggal.setText(identitasModel.tanggal_lahir);
        txt_kebangsaan.setText(identitasModel.kebangsaan);
        txt_jk.setText(identitasModel.jenis_kelamin);
        txt_merried.setText(identitasModel.merried);
        txt_agama.setText(identitasModel.agama);
    }

    @Override
    public void onBackPressed() {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            Refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

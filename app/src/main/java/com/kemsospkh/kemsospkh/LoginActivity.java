package com.kemsospkh.kemsospkh;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kemsospkh.kemsospkh.app.AppController;
import com.kemsospkh.kemsospkh.helper.DatabaseHelper;
import com.kemsospkh.kemsospkh.util.Server;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    Button btn_login, btn_register;
    EditText txt_username, txt_password;
    Intent intent;

    boolean success;
    ConnectivityManager conMgr;

    private String url = Server.URL + "login";

    private static final String TAG = LoginActivity.class.getSimpleName();

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MSG = "msg";

    //private final static String TAG_USERNAME = "username";
    private final static String TAG_NIK = "nik";
    private final static String TAG_ID_PERSONAL = "idpersonal";

    String tag_jObj_obj = "json_obj_req";

    SharedPreferences sharedPreferences;
    Boolean session = false;
    String idpersonal, username;

    private DatabaseHelper database;

    public static final String my_shared_preferences = "my_shared_preferences";
    public static final String session_status = "session_status";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        {
            if (conMgr.getActiveNetworkInfo() != null
                    && conMgr.getActiveNetworkInfo().isAvailable()
                    && conMgr.getActiveNetworkInfo().isConnected()) {

            } else {
                Toast.makeText(getApplicationContext(), "Periksa koneksi internet",
                        Toast.LENGTH_LONG).show();
            }
        }

        btn_register = (Button) findViewById(R.id.btn_register);
        btn_login = (Button) findViewById(R.id.btn_login);
        txt_username = (EditText) findViewById(R.id.txt_username);

        // sementara nanti dihapus
        txt_username.setText("3509182111900001"); ////
        /// ---------///

        txt_password = (EditText) findViewById(R.id.txt_password);

        // sementara nanti dihapus
        txt_password.setText("oivFM7"); ////
        /// ---------///

        database = new DatabaseHelper(this);

        // cek session login jika TRUE langsung menjalankan MainActivity.class
        sharedPreferences = getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        session = sharedPreferences.getBoolean(session_status, false);
        idpersonal = sharedPreferences.getString(TAG_ID_PERSONAL, null);
        username = sharedPreferences.getString(TAG_NIK, null);

        if (session) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.putExtra(TAG_ID_PERSONAL, idpersonal);
            intent.putExtra(TAG_NIK, username);
            finish();
            startActivity(intent);
        }

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String username = txt_username.getText().toString();
                final String password = txt_password.getText().toString();

                // mengecek kolom kosong
                if (username.trim().length() > 0 && password.trim().length() > 0) {
                    if (conMgr.getActiveNetworkInfo() != null
                            && conMgr.getActiveNetworkInfo().isAvailable()
                            && conMgr.getActiveNetworkInfo().isConnected()) {
                        checkLogin(username, password);
                    }
                }
            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent register = new Intent(LoginActivity.this, RegristrasiActivity.class);
                finish();
                startActivity(register);
            }
        });
    }

    private void checkLogin(final String nik, final String idpersonal) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Login in ... ");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Login Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getBoolean(TAG_SUCCESS);

                    // mengecek eror pada node json
                    if (success == true) {
                        String nik = jObj.getString(TAG_NIK);
                        String idpersonal = jObj.getString(TAG_ID_PERSONAL);

                        Log.e("Successfully Login!", jObj.toString());
                        Toast.makeText(getApplicationContext(), jObj.getString(TAG_MSG), Toast.LENGTH_LONG).show();

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean(session_status, true);
                        editor.putString(TAG_ID_PERSONAL, idpersonal);
                        editor.putString(TAG_NIK, nik);
                        finish();
                        editor.commit();


                        // memanggil main activity
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra(TAG_ID_PERSONAL, idpersonal);
                        intent.putExtra(TAG_NIK, nik);
                        fetchData();
                        finish();
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), jObj.getString(TAG_MSG),
                                Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Eror: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(),
                        Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("nik", nik);
                params.put("password", idpersonal);

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(strReq, tag_jObj_obj);
    }

    private void fetchData() {
        database = new DatabaseHelper(this);
        try {

            database.createDataBase();
            database.openDataBase();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if (!progressDialog.isShowing())
            progressDialog.dismiss();
    }
}

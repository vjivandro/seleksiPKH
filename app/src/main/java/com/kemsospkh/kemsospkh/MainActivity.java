package com.kemsospkh.kemsospkh;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.kemsospkh.kemsospkh.helper.DatabaseHelper;
import com.kemsospkh.kemsospkh.model.IdentitasModel;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    String nik, idpersonal, email;
    TextView txt_nik, nav_nik, nav_email;
    private IdentitasModel identitasModel;
    SharedPreferences sharedPreferences;

    private static final String TAG_NIK = "nik";
    private static final String TAG_EMAIL = "email";

    public static final String my_shared_preferences = "my_shared_preferences";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPreferences = getSharedPreferences(LoginActivity.my_shared_preferences, Context.MODE_PRIVATE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);

        nav_nik = (TextView)header.findViewById(R.id.nav_nik);
        txt_nik = (TextView) findViewById(R.id.tv_nama);
        nav_email = (TextView) findViewById(R.id.nav_email);

        nik = getIntent().getStringExtra(TAG_NIK);
        email = getIntent().getStringExtra(TAG_EMAIL);
        txt_nik.setText(nik);
        nav_nik.setText(nik);
//        nav_email.setText(email);
//
       DatabaseHelper db = new DatabaseHelper(this);
       IdentitasModel identitasModel = db.getDataIdentitas();
       txt_nik.setText(identitasModel.nik);
       nav_nik.setText(identitasModel.nik);
//  //      nav_email.setText(biodata.email);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;
        } else if(id == R.id.action_logout) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(LoginActivity.session_status, false);
            editor.putString(TAG_NIK, null);
            editor.commit();

            Intent inten = new Intent(MainActivity.this, LoginActivity.class);
            finish();
            startActivity(inten);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_lowongan) {

        } else if (id == R.id.nav_profil) {
            // Handle the profil action
            Intent profil = new Intent(MainActivity.this, ProfilActivity.class);
            startActivity(profil);
        } else if (id == R.id.nav_ktp) {
            // Handle the profil action
            Intent ktp = new Intent(MainActivity.this, IdentitasActivity.class);
            ktp.putExtra(TAG_NIK, nik);
            startActivity(ktp);

        } else {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

package com.kemsospkh.kemsospkh;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kemsospkh.kemsospkh.helper.DatabaseHelper;
import com.kemsospkh.kemsospkh.model.ProfilModel;

public class ProfilActivity extends AppCompatActivity{
    TextView txt_nama, txt_email, txt_handphone, txt_alamat, txt_provinsi, txt_kabupaten, txt_kecamatan;
    TextView txt_tambah, lihat_ktp, lihat_ijasah, lihat_domisili;
    String idpersonal, nik, nama, tempat_lahir, tanggal_lahir;
    public ImageView image_ktp, image_ijasah, image_domisili, viewktp, viewijasah, viewdomisili;
    Dialog mDialog;
    Cursor cursor;

    public static final String TAG_NAMA = "nama";
    public static final String TAG_NIK = "nik";
    public static final String TAG_TEMPAT_LAHIR = "tempat_lahir";
    public static final String TAG_TANGGAL_LAHIR = "tanggal_lahir";

    public static final String COLUMN_FTKP = "foto_ktp";
    public static final String COLUMN_FIJASAH = "foto_ijasah";
    public static final String COLUMN_FDOMISILI = "foto_domisili";

    public static final String my_shared_preferences = "my_shared_preferences";

    SharedPreferences sharedPreferences;
    public DatabaseHelper db = new DatabaseHelper((this));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_profil);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txt_nama = (TextView) findViewById(R.id.tv_nama);
        txt_email = (TextView) findViewById(R.id.tv_email);
        txt_handphone = (TextView) findViewById(R.id.tv_ponsel);
        txt_alamat = (TextView) findViewById(R.id.tv_alamat);
        txt_provinsi = (TextView) findViewById(R.id.tv_provinsi);
        txt_kabupaten = (TextView) findViewById(R.id.tv_kabuaten);
        txt_kecamatan = (TextView) findViewById(R.id.tv_kecamatan);
        lihat_domisili = (TextView) findViewById(R.id.tv_fotoDomisili);

        sharedPreferences = getSharedPreferences(LoginActivity.my_shared_preferences, Context.MODE_PRIVATE);

//        nik = getIntent().getStringExtra(TAG_NIK);
//        txt_nik.setText(nik);

        txt_tambah = (TextView) findViewById(R.id.tambah);
        txt_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent tambah = new Intent(ProfilActivity.this, TambahProfilActivity.class);
                tambah.putExtra(TAG_NIK, nik);
                startActivity(tambah);
            }
        });

        lihat_domisili.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfilModel profil = db.getDataProfil();
                mDialog = new Dialog(ProfilActivity.this);
                mDialog.setContentView(R.layout.foto_dialog);
                image_domisili = (ImageView)mDialog.findViewById(R.id.imageView2);
                image_domisili.setImageBitmap(BitmapFactory.decodeByteArray(profil.foto_domisili, 0, profil.foto_domisili.length));
                mDialog.show();
            }
        });



        ProfilModel profil = db.getDataProfil();

        txt_nama.setText(profil.nama);
        txt_email.setText(profil.email);
        txt_handphone.setText(profil.handphone);
        txt_alamat.setText(profil.alamat);
    }


    public void Refresh(){
        DatabaseHelper db = new DatabaseHelper((this));
        ProfilModel profil = db.getDataProfil();

        txt_nama.setText(profil.nama);
        txt_email.setText(profil.email);
        txt_handphone.setText(profil.handphone);
        txt_alamat.setText(profil.alamat);
    }

    @Override
    public void onBackPressed() {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            Refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package com.kemsospkh.kemsospkh;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.kemsospkh.kemsospkh.adapter.IdentitasAdapter;
import com.kemsospkh.kemsospkh.helper.DatabaseHelper;
import com.kemsospkh.kemsospkh.model.IdentitasModel;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class TambahIdentitasActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener{

    protected Cursor cursor;
    private DatePickerDialog fromDatePickerDialog;
    private String nik, alamatKtp, tempat_lahir, tanggal_lahir, jenis_kelamin, kebangsaan, merried, agama;
    private DatabaseHelper db;
    private IdentitasModel identitasModel;
    private IdentitasAdapter data;
    private Bitmap bp;
    private byte[] foto_ktp;
    private SimpleDateFormat dateFormatter;

    private EditText edt_nik, edt_alamtKtp, edt_tempat, edt_tanggallahir, edt_jk, edt_kebangsaan, edt_agama, edt_status;
    private ImageButton pic_ktp;
    private RadioGroup rg_jenisKalmin, rg_kebangsaan;
    private RadioButton male, female, wni, wna;
    private Spinner agama_spin, statuskawin_spin;
    Button simpan;

    ArrayAdapter<CharSequence> adapter;

    SharedPreferences sharedPreferences;

    private static final String TAG_NIK = "nik";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_identitas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreferences = getSharedPreferences(IdentitasActivity.my_shared_preferences, Context.MODE_PRIVATE);

        db = new DatabaseHelper(this);
        edt_nik = (EditText) findViewById(R.id.edt_nik);
        nik = getIntent().getStringExtra(TAG_NIK);
        edt_nik.setText(nik);
        edt_alamtKtp = (EditText) findViewById(R.id.edt_almKtp);
        edt_alamtKtp.setText("Alamat Sesuai Domisili");
        edt_tempat = (EditText) findViewById(R.id.edt_tempat_lahir);
        edt_tanggallahir = (EditText) findViewById(R.id.edt_tanggal_lahir);
        edt_jk = (EditText) findViewById(R.id.edt_jk);
        edt_kebangsaan = (EditText) findViewById(R.id.edt_kebangsaan);
        edt_agama = (EditText) findViewById(R.id.edt_agama);
        edt_status = (EditText) findViewById(R.id.edt_status);
        pic_ktp = (ImageButton) findViewById(R.id.ktp);
        simpan = (Button) findViewById(R.id.simpan);

        // Radio Group Jenis Kelamin
        rg_jenisKalmin = (RadioGroup) findViewById(R.id.rg_jk);
        male =(RadioButton) findViewById(R.id.rb_male);
        female = (RadioButton) findViewById(R.id.rb_female);

        // Radio Group Kebangsaan
        rg_kebangsaan = (RadioGroup) findViewById(R.id.rg_warga);
        wni =(RadioButton) findViewById(R.id.rb_wni);
        wna = (RadioButton) findViewById(R.id.rb_wna);

        // Status agama spinner
        agama_spin = (Spinner) findViewById(R.id.agamaSpinner);
        adapter = ArrayAdapter.createFromResource(this,
                R.array.agama_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        agama_spin.setAdapter(adapter);
        agama_spin.setOnItemSelectedListener(this);

        // Status kawin spinner
        statuskawin_spin = (Spinner) findViewById(R.id.statusSpinner);
        adapter = ArrayAdapter.createFromResource(this,
                R.array.status_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statuskawin_spin.setAdapter(adapter);
        statuskawin_spin.setOnItemSelectedListener(this);

        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        setDateTimeField();
    }

    //Calender tanggal_lahir
    private void setDateTimeField() {
        edt_tanggallahir.setOnClickListener(this);
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                edt_tanggallahir.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onClick(View view) {
        if (view == edt_tanggallahir) {
            fromDatePickerDialog.show();
        }
    }

    // Radiobutton jenis_kelamin
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.rb_male:
                if (checked)
                    // Pirates are the best
                    edt_jk.setText("Laki - Laki");
                    break;
            case R.id.rb_female:
                if (checked)
                    // Ninjas rule
                    edt_jk.setText("Perempuan");
                    break;
        }
    }

    // Radiobutton jenis_kelamin
    public void onClickKebangsaan(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.rb_wni:
                if (checked)
                    // Pirates are the best
                    edt_kebangsaan.setText("WNI");
                break;
            case R.id.rb_wna:
                if (checked)
                    // Ninjas rule
                    edt_kebangsaan.setText("WNA");
                break;
        }
    }

    // Spinner Function
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {

        edt_agama.setText(String.valueOf(agama_spin.getSelectedItem()));

        edt_status.setText(String.valueOf(statuskawin_spin.getSelectedItem()));
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void buttonClicked(View view){
        int id = view.getId();

        switch (id){
            case R.id.simpan:
                if (edt_nik.getText().toString().trim().equals("") || edt_alamtKtp.getText().toString().trim().equals("")||
                        edt_tempat.getText().toString().trim().equals("") || edt_tanggallahir.getText().toString().trim().equals("")||
                        edt_jk.getText().toString().trim().equals("") || edt_kebangsaan.getText().toString().trim().equals("") ||
                        edt_status.getText().toString().trim().equals("") || edt_agama.getText().toString().trim().equals("")){
                    Toast.makeText(getApplicationContext(),
                            "Harus terisi semua", Toast.LENGTH_LONG).show();
                } else {
                    insertIdentitas();
                    blank();
                }
                break;
            case R.id.ktp:
                selectImage();
                break;
        }
    }

    private void selectImage() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent photoPickerIntent) {
        switch(requestCode) {
            case 2:
                if(resultCode == RESULT_OK){
                    Uri choosenImage = photoPickerIntent.getData();

                    if(choosenImage != null){

                        bp = decodeUri(choosenImage, 1000);
                        pic_ktp.setImageBitmap(bp);
                    }
                }
                break;
        }
    }

    private Bitmap decodeUri(Uri choosenImage, int REQUIRED_SIZE) {
        try {

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(getContentResolver().openInputStream(choosenImage), null, o);

            // The new size we want to scale to
            // final int REQUIRED_SIZE =  size;

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 2;
            while (true) {
                if (width_tmp / 4 < REQUIRED_SIZE
                        || height_tmp / 4 < REQUIRED_SIZE) {
                    break;
                }
                width_tmp /= 4;
                height_tmp /= 4;
                scale *= 4;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(getContentResolver().openInputStream(choosenImage), null, o2);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    //Convert bitmap to bytes
    private byte[] profileImage(Bitmap bmp){

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 0, bos);

        return bos.toByteArray();
    }

    // function to get values from the Edittext and image
    private void getValues(){
        nik = edt_nik.getText().toString();
        alamatKtp = edt_alamtKtp.getText().toString();
        tempat_lahir = edt_tempat.getText().toString();
        tanggal_lahir = edt_tanggallahir.getText().toString();
        jenis_kelamin = edt_jk.getText().toString();
        kebangsaan = edt_kebangsaan.getText().toString();
        merried = edt_status.getText().toString();
        agama = edt_agama.getText().toString();
        foto_ktp = profileImage(bp);
    }

    private void insertIdentitas() {
        getValues();

        db.insertIdentitas(new IdentitasModel(nik, alamatKtp, tempat_lahir, tanggal_lahir, jenis_kelamin,kebangsaan, merried, agama, foto_ktp));
        Toast.makeText(getApplicationContext(), "Data Profile Berhasil Disimpan", Toast.LENGTH_LONG).show();
    }

    // Make blank all Edit Text
    private void blank() {
        edt_nik.setText(null);
        edt_alamtKtp.setText(null);
        edt_tempat.setText(null);
        edt_tanggallahir.setText(null);
        edt_jk.setText(null);
        edt_kebangsaan.setText(null);
        edt_status.setText(null);
        edt_agama.setText(null);
        pic_ktp.setImageBitmap(null);
    }
}

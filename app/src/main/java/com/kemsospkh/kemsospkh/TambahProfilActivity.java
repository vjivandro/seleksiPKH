package com.kemsospkh.kemsospkh;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.kemsospkh.kemsospkh.adapter.ProfilAdapter;
import com.kemsospkh.kemsospkh.adapter.WilayahAdapter;
import com.kemsospkh.kemsospkh.helper.DatabaseHelper;
import com.kemsospkh.kemsospkh.model.ProfilModel;
import com.kemsospkh.kemsospkh.model.WilayahModel;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TambahProfilActivity extends AppCompatActivity {

    private static final String TAG = TambahProfilActivity.class.getSimpleName();
    private EditText edt_nama, edt_email, edt_handphone, edt_alamat, edt_wilayah, edt_kode;
    private ImageView pic_domisili;
    private AutoCompleteTextView acttext_prov, acttext_kab, acttext_kec;
    private DatabaseHelper database;
    private String nama, email, ponsel, alamat, kodeWilayah,wilayah;
    private ProfilModel profilModel;
    private ProfilAdapter data;
    private Bitmap bp;
    private byte[] foto_ktp, foto_ijasah, foto_domisili;
    private ImageButton cari_wilayah;

    SharedPreferences sharedPreferences;

    ListView listView;
    List<WilayahModel> itemList = new ArrayList<WilayahModel>();
    WilayahAdapter wilayahAdapter;

    public static final String TAG_PROVINSI = "provinsi";
    public static final String TAG_KABUPATEN = "kabupaten";
    public static final String TAG_KECAMATAN = "kecamatan";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_profil);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreferences = getSharedPreferences(LoginActivity.my_shared_preferences, Context.MODE_PRIVATE);

        database = new DatabaseHelper(this);

        edt_nama = (EditText) findViewById(R.id.edt_nama);
        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_handphone = (EditText) findViewById(R.id.edt_hanphone);
        edt_alamat = (EditText) findViewById(R.id.edt_alamat);
        edt_wilayah = (EditText) findViewById(R.id.edt_wilayah);
        edt_kode = (EditText) findViewById(R.id.edt_kode);
        pic_domisili = (ImageView) findViewById(R.id.domisili);
        cari_wilayah = (ImageButton) findViewById(R.id.button_search);
        cari_wilayah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cari = new Intent(TambahProfilActivity.this, WilayahActivity.class);
                startActivity(cari);
            }
        });

        WilayahModel wil = database.getDataWil();
        edt_wilayah.setText(wil.getLabel());
        edt_kode.setText(wil.getKdwilayah());
    }

    public void buttonClicked(View view){
        int id = view.getId();

        switch (id){
            case R.id.button_simpan:
                if (edt_nama.getText().toString().trim().equals("") || edt_email.getText().toString().trim().equals("")||
                        edt_handphone.getText().toString().trim().equals("") || edt_alamat.getText().toString().trim().equals("")||
                        edt_wilayah.getText().toString().trim().equals("")){
                    Toast.makeText(getApplicationContext(),
                            "Harus terisi semua", Toast.LENGTH_LONG).show();
                } else {
                    insertProfil();
                    blank();
                }
                break;
            case R.id.domisili:
                selectImage();
                break;
        }
    }


    private void selectImage() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent photoPickerIntent) {
        switch(requestCode) {
            case 2:
                if(resultCode == RESULT_OK){
                    Uri choosenImage = photoPickerIntent.getData();

                    if(choosenImage != null){

                        bp = decodeUri(choosenImage, 1000);
                        pic_domisili.setImageBitmap(bp);
                    }
                }
                break;
        }
    }

    private Bitmap decodeUri(Uri choosenImage, int REQUIRED_SIZE) {
        try {

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(getContentResolver().openInputStream(choosenImage), null, o);

            // The new size we want to scale to
            // final int REQUIRED_SIZE =  size;

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 2;
            while (true) {
                if (width_tmp / 4 < REQUIRED_SIZE
                        || height_tmp / 4 < REQUIRED_SIZE) {
                    break;
                }
                width_tmp /= 4;
                height_tmp /= 4;
                scale *= 4;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(getContentResolver().openInputStream(choosenImage), null, o2);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    //Convert bitmap to bytes
    private byte[] profileImage(Bitmap bmp){

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 0, bos);

        return bos.toByteArray();
    }

    // function to get values from the Edittext and image
    private void getValues(){
        nama = edt_nama.getText().toString();
        email = edt_email.getText().toString();
        ponsel = edt_handphone.getText().toString();
        alamat = edt_alamat.getText().toString();
        wilayah = edt_wilayah.getText().toString();
        foto_domisili = profileImage(bp);
    }

    private void insertProfil() {
        getValues();

        database.insertProfil(new ProfilModel(nama, email, ponsel, alamat, kodeWilayah, wilayah, foto_domisili));
        Toast.makeText(getApplicationContext(), "Data Profile Berhasil Disimpan", Toast.LENGTH_LONG).show();
    }

    // Make blank all Edit Text
    private void blank() {
        edt_nama.setText(null);
        edt_email.setText(null);
        edt_handphone.setText(null);
        edt_alamat.setText(null);
        edt_wilayah.setText(null);
        pic_domisili.setImageBitmap(null);
    }
}
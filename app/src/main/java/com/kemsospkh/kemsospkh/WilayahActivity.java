package com.kemsospkh.kemsospkh;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.kemsospkh.kemsospkh.adapter.WilayahAdapter;
import com.kemsospkh.kemsospkh.helper.DatabaseHelper;
import com.kemsospkh.kemsospkh.model.WilayahModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WilayahActivity extends AppCompatActivity {

    ListView listView;
    EditText cari, kode;
    List<WilayahModel> itemList = new ArrayList<WilayahModel>();
    WilayahAdapter wilayahAdapter;
    EditText searchView;
    DatabaseHelper database = new DatabaseHelper(this);

    public String kode_wil, provinsi, kabupaten, kecamatan, kdwilayah, label;

    public static final String TAG_KDWIL = "kdwilayah";
    public static final String TAG_PROVINSI = "provinsi";
    public static final String TAG_KABUPATEN = "kabupaten";
    public static final String TAG_KECAMATAN = "kecamatan";


    private final static String TAG = WilayahActivity.class.getName().toString();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wilayah);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        database = new DatabaseHelper(getApplicationContext());

        listView = (ListView) findViewById(R.id.list);
        cari = (EditText) findViewById(R.id.cari_wilayah);
        kode = (EditText) findViewById(R.id.cari_kode);
        searchView = (EditText) findViewById(R.id.search);

        wilayahAdapter = new WilayahAdapter(WilayahActivity.this, itemList);
        listView.setAdapter(wilayahAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                pilihan(i);
            }
        });

        getWilayah();

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void getSearch(String s) {

    }

    private void pilihan(int i) {
        kode_wil = itemList.get(i).getKdwilayah();
        provinsi = itemList.get(i).getProvinsi();
        kabupaten = itemList.get(i).getKabupaten();
        kecamatan = itemList.get(i).getKecamatan();

        cari.setText(provinsi + "" + kabupaten + "" + kecamatan);
        kode.setText(kode_wil);

        Toast.makeText(getApplicationContext(), "pilihan " + provinsi + kabupaten + kecamatan, Toast.LENGTH_LONG).show();
        if (cari.getText().toString().equals("") || kode.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(),
                    "Harus pilih wilayah ", Toast.LENGTH_LONG).show();
        } else {
            insertWilayah();
        }
    }

    private void getWilayah() {
        ArrayList<HashMap<String, String>> row = database.getWilayah();

        for (int i = 0; i < row.size(); i++) {
            String kode = row.get(i).get(TAG_KDWIL);
            String provinsi = row.get(i).get(TAG_PROVINSI);
            String kabupaten = row.get(i).get(TAG_KABUPATEN);
            String kecamatan = row.get(i).get(TAG_KECAMATAN);

            WilayahModel wilayahModel = new WilayahModel();

            wilayahModel.setKdwilayah(kode);
            wilayahModel.setProvinsi(provinsi + ", ");
            wilayahModel.setKabupaten(kabupaten + ", ");
            wilayahModel.setKecamatan(kecamatan);

            itemList.add(wilayahModel);
        }

        wilayahAdapter.notifyDataSetChanged();
    }

    private void getValues() {
        cari.setText(provinsi + "" + kabupaten + "" + kecamatan);
        kode.setText(kode_wil);

        kdwilayah = kode.getText().toString();
        label = cari.getText().toString();
    }

    private void insertWilayah() {
        getValues();

        database.insertWilayah(new WilayahModel(kdwilayah, label));
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWilayah();
    }
}
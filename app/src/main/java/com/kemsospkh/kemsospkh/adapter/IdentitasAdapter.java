package com.kemsospkh.kemsospkh.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kemsospkh.kemsospkh.R;
import com.kemsospkh.kemsospkh.model.IdentitasModel;

import java.util.ArrayList;

/**
 * Created by ivandro on 3/28/18.
 */

public class IdentitasAdapter extends ArrayAdapter<IdentitasModel> {
    Context context;
    ArrayList<IdentitasModel> identitasModels = new ArrayList<IdentitasModel>();

    public IdentitasAdapter(Context context, ArrayList<IdentitasModel> identitasModels) {
        super(context, R.layout.activity_ktp, identitasModels);
        this.context = context;
        this.identitasModels = identitasModels;
    }

    public class Holder {
        TextView tv_nik;
        TextView tv_alamatKtp;
        TextView tv_tempatlahir;
        TextView tv_tanggallahir;
        TextView tv_jk;
        TextView tv_kebangsaan;
        TextView tv_merried;
        TextView tv_agama;
        ImageView image_ktp;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        IdentitasModel identity = getItem(position);
        Holder viewHolder;

        if (convertView == null){

            viewHolder = new Holder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.activity_ktp, parent, false);

            viewHolder.tv_nik = (TextView) convertView.findViewById(R.id.tv_nik);
            viewHolder.tv_alamatKtp = (TextView) convertView.findViewById(R.id.tv_almKtp);
            viewHolder.tv_tempatlahir = (TextView) convertView.findViewById(R.id.tv_tempat);
            viewHolder.tv_tanggallahir = (TextView) convertView.findViewById(R.id.tv_tanggal);
            viewHolder.tv_jk = (TextView) convertView.findViewById(R.id.tv_jeniskelamin);
            viewHolder.tv_kebangsaan = (TextView) convertView.findViewById(R.id.tv_kebangsaan);
            viewHolder.tv_merried = (TextView) convertView.findViewById(R.id.tv_statuskawin);
            viewHolder.tv_agama = (TextView) convertView.findViewById(R.id.tv_agama);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (Holder) convertView.getTag();
        }

        viewHolder.tv_nik.setText(identity.getNik());
        viewHolder.tv_alamatKtp.setText(identity.getAlamatKtp());
        viewHolder.tv_tempatlahir.setText(identity.getTempat_lahir());
        viewHolder.tv_tanggallahir.setText(identity.getTanggal_lahir());
        viewHolder.tv_jk.setText(identity.getJenis_kelamin());
        viewHolder.tv_kebangsaan.setText(identity.getKebangsaan());
        viewHolder.tv_merried.setText(identity.getMerried());
        viewHolder.tv_agama.setText(identity.getAgama());
        viewHolder.image_ktp.setImageBitmap(convertToBitmap(identity.getFoto_ktp()));
        return convertView;
    }

    private Bitmap convertToBitmap(byte[] foto_ktp) {
        return BitmapFactory.decodeByteArray(foto_ktp, 0, foto_ktp.length);
    }
}

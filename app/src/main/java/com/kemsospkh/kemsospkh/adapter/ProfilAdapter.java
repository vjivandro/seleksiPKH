package com.kemsospkh.kemsospkh.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kemsospkh.kemsospkh.R;
import com.kemsospkh.kemsospkh.model.ProfilModel;
import com.kemsospkh.kemsospkh.model.WilayahModel;

import java.util.ArrayList;

/**
 * Created by ivandro on 3/25/18.
 */

public class ProfilAdapter extends ArrayAdapter<ProfilModel> {
    Context context;
    ArrayList<ProfilModel> mprofil = new ArrayList<ProfilModel>();

    public ProfilAdapter(Context context, ArrayList<ProfilModel> mprofil) {
        super(context, R.layout.content_profil, mprofil);
        this.context = context;
        this.mprofil = mprofil;
    }

    public class Holder {
        TextView tv_nama;
        TextView tv_email;
        TextView tv_ponsel;
        TextView tv_alamat;
        TextView tv_provinsi;
        TextView tv_kabupaten;
        TextView tv_kecamatan;
        ImageView image_domisili;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        ProfilModel data = getItem(position);
        Holder viewHolder;

        if (convertView == null){

            viewHolder = new Holder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.content_profil, parent, false);

            viewHolder.tv_nama = (TextView) convertView.findViewById(R.id.tv_nama);
            viewHolder.tv_email = (TextView) convertView.findViewById(R.id.tv_email);
            viewHolder.tv_ponsel = (TextView) convertView.findViewById(R.id.tv_ponsel);
            viewHolder.tv_alamat = (TextView) convertView.findViewById(R.id.tv_alamat);
            viewHolder.tv_provinsi = (TextView) convertView.findViewById(R.id.tv_provinsi);
            viewHolder.tv_kabupaten = (TextView) convertView.findViewById(R.id.tv_kabuaten);
            viewHolder.tv_kecamatan = (TextView) convertView.findViewById(R.id.tv_kecamatan);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (Holder) convertView.getTag();
        }

        viewHolder.tv_nama.setText(data.getNama());
        viewHolder.tv_email.setText(data.getEmail());
        viewHolder.tv_ponsel.setText(data.getHandphone());
        viewHolder.tv_alamat.setText(data.getAlamat());
        viewHolder.tv_provinsi.setText(data.getProvinsi());
        viewHolder.tv_kabupaten.setText(data.getKabupaten());
        viewHolder.tv_kecamatan.setText(data.getKecamatan());
        viewHolder.image_domisili.setImageBitmap(convertToBitmap(data.getFoto_domisili()));
        return convertView;
    }

    private Bitmap convertToBitmap(byte[] foto_domisili) {
        return BitmapFactory.decodeByteArray(foto_domisili, 0, foto_domisili.length);
    }
}

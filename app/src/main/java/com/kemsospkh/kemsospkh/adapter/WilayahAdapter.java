package com.kemsospkh.kemsospkh.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kemsospkh.kemsospkh.R;
import com.kemsospkh.kemsospkh.model.WilayahModel;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by ivandro on 3/30/18.
 */

public class WilayahAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<WilayahModel> items;

    public TextView kd, provinsi, kabupaten, kecamatan;

    public WilayahAdapter(Activity activity, List<WilayahModel> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row, null);
        }

        kd = (TextView) convertView.findViewById(R.id.kdwil);
        provinsi = (TextView) convertView.findViewById(R.id.provinsi);
        kabupaten = (TextView) convertView.findViewById(R.id.kabupaten);
        kecamatan = (TextView) convertView.findViewById(R.id.kecamatan);

        WilayahModel wilayahModel = items.get(position);

        kd.setText(wilayahModel.getKdwilayah());
        provinsi.setText(wilayahModel.getProvinsi());
        kabupaten.setText(wilayahModel.getKabupaten());
        kecamatan.setText(wilayahModel.getKecamatan());

        return convertView;
    }
}

package com.kemsospkh.kemsospkh.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.kemsospkh.kemsospkh.model.IdentitasModel;
import com.kemsospkh.kemsospkh.model.WilayahModel;
import com.kemsospkh.kemsospkh.model.ProfilModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Provider;
import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ivandro on 3/17/18.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName();
    private static final int DATABASE_VERSION = 2;

    // path database sqlite
    private static String DATABASE_PATH = "data/data/com.kemsospkh.kemsospkh/databases/";
    // nama Database
    public static final String DATABASE_NAME = "sdmpkh.db";

    //tabel profilDAO
    public static final String TABLE_PROFIL = "profilDAO";
    public static final String COLUMN_ID = "id_pro";
    public static final String COLUMN_NAMA = "nama";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_HANDPHONE = "ponsel";
    public static final String COLUMN_ALAMAT = "alamat";
    public static final String COLUMN_KDWIL = "kdwilayah";
    public static final String COLUMN_WILAYAH = "wilayah";
    public static final String COLUMN_FDOMISILI = "filedomisili";

    //tabel identitas
    public static final String TABLE_IDENTITAS = "identitasDAO";
    public static final String COLUMN_NIK = "nik";
    public static final String COLUMN_ALAMATKTP = "alamatKtp";
    public static final String COLUMN_TEMAPAT_LAHIR = "tempat_lahir";
    public static final String COLUMN_TANGGAL_LAHIR = "tanggal_lahir";
    public static final String COLUMN_JK = "jenis_kelamin";
    public static final String COLUMN_KEBANGSAAAN = "kebangsaan";
    public static final String COLUMN_MERRIED = "merried";
    public static final String COLUMN_AGAMA = "agama";
    public static final String COLUMN_FKTP = "foto_ktp";

    //Tabel wilayah
    public static final String TABLE_WILAYAH = "wilayahDAO";
    public static final String COLUMN_KODE = "kdwilayah";
    public static final String COLUMN_PROVINSI = "provinsi";
    public static final String COLUMN_KABUPATEN = "kabupaten";
    public static final String COLUMN_KECAMATAN = "kecamatan";

    //Tabel wilayah
    public static final String TABLE_PILIHWILAYAH = "wilayahTerpilih";
    public static final String COLUMN_KD = "kdwil";
    public static final String COLUMN_LABEL = "wilayah";

    private SQLiteDatabase database;
    private Context mContext;

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
    }

    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();
        if (dbExist) {
        }
        else {
            this.getWritableDatabase();
            try {
                copyDataBase();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean checkDataBase() {

        File dbFile = null;
        try {
            // String myPath = DATABASE_PATH + DATABASE_NAME;
            // checkDB = SQLiteDatabase.openDatabase(myPath, null,
            // SQLiteDatabase.OPEN_READONLY);
            dbFile = new File(DATABASE_PATH + DATABASE_NAME);
            return dbFile.exists();
        } catch (Exception e) {
            e.printStackTrace();
        }
		/*if(checkDB != null){
		 checkDB.close();}
		 return checkDB != null ? true : false;*/
        return dbFile != null ? true : false;
    }

    /**
     * Copies your database from your local assets-folder to the just created
     * empty database in the system folder, from where it can be accessed and
     * handled. This is done by transfering bytestream.
     * */
    private void copyDataBase() throws IOException {

        InputStream myInput = mContext.getAssets().open(DATABASE_NAME);
        String outFileName = DATABASE_PATH + DATABASE_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    public void openDataBase() throws SQLException {

        String myPath = DATABASE_PATH + DATABASE_NAME;
        database = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READWRITE);
        close();
    }

    @Override
    public synchronized void close() {
        if (database != null && database.isOpen()) {
            database.close();
            super.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

//    @Override
//    public void onCreate(SQLiteDatabase db){
//        final String CREATE_TABLE_PROFIL = "CREATE TABLE " + TABLE_PROFIL + "(" +
//                COLUMN_ID + " INTEGER PRIMARY KEY autoincrement, " +
//                COLUMN_NAMA + " TEXT NOT NULL, " +
//                COLUMN_EMAIL + " TEXT NOT NULL, " +
//                COLUMN_HANDPHONE + " TEXT NOT NULL, " +
//                COLUMN_ALAMAT + " TEXT NOT NULL, " +
//                COLUMN_KEC + " TEXT NOT NULL, " +
//                COLUMN_FDOMISILI + " BLOB NOT NULL " +
//                " )";
//
//        db.execSQL(CREATE_TABLE_PROFIL);
//
//        final String CREATE_TABLE_DATAKTP = "CREATE TABLE "+ TABLE_IDENTITAS + "(" +
//                COLUMN_NIK + " INTEGER PRIMARY KEY, " +
//                COLUMN_ALAMATKTP + " TEXT NOT NULL, " +
//                COLUMN_TEMAPAT_LAHIR + " TEXT NOT NULL, " +
//                COLUMN_TANGGAL_LAHIR + " TEXT NOT NULL, " +
//                COLUMN_JK + " TEXT NOT NULL, " +
//                COLUMN_KEBANGSAAAN + " TEXT NOT NULL, " +
//                COLUMN_MERRIED + " TEXT NOT NULL, " +
//                COLUMN_AGAMA + " TEXT NOT NULL, " +
//                COLUMN_FKTP + " BLOB NOT NULL " +
//                ")";
//        db.execSQL(CREATE_TABLE_DATAKTP);
//
//        final String CREATE_TABLE_WILAYAH = "CREATE TABLE "+ TABLE_WILAYAH + "(" +
//                COLUMN_KODE + " TEXT NOT NULL, " +
//                COLUMN_PROVINSI + " TEXT NOT NULL, " +
//                COLUMN_KABUPATEN + " TEXT NOT NULL " +
//                COLUMN_KECAMATAN + " TEXT NOT NULL " +
//                ')';
//        db.execSQL(CREATE_TABLE_WILAYAH);
//    }
//
//    @Override
//    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        //DROP TABEL PROFIL
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROFIL);
//        onCreate(db);
//
//        //DROP TABEL KTP
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IDENTITAS);
//        onCreate(db);
//
//        //DROP TABEL KECAMATAN
//        db.execSQL("DROP TABLE IF EXiSTS " + TABLE_WILAYAH);
//        onCreate(db);
//
//    }

    // menampilkan semua data dari tabel profilDAO
    public ProfilModel getDataProfil(){
        ProfilModel profil = new ProfilModel();
        database = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PROFIL;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0){
            cursor.moveToFirst();

            profil.nama = cursor.getString(cursor.getColumnIndex(COLUMN_NAMA));
            profil.email = cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL));
            profil.handphone = cursor.getString(cursor.getColumnIndex(COLUMN_HANDPHONE));
            profil.alamat = cursor.getString(cursor.getColumnIndex(COLUMN_ALAMAT));
            profil.kodeWilayah = cursor.getString(cursor.getColumnIndex(COLUMN_KDWIL));
            profil.wilayah = cursor.getString(cursor.getColumnIndex(COLUMN_WILAYAH));
            profil.foto_domisili = cursor.getBlob(cursor.getColumnIndex(COLUMN_FDOMISILI));
        }
        cursor.close();
        return profil;
    }

    // menampilkan semua data dari tabel ktpDAO
    public IdentitasModel getDataIdentitas(){
        IdentitasModel ktp = new IdentitasModel();
        database = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_IDENTITAS;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0){
            cursor.moveToFirst();

            ktp.nik = cursor.getString(cursor.getColumnIndex(COLUMN_NIK));
            ktp.alamatKtp = cursor.getString(cursor.getColumnIndex(COLUMN_ALAMATKTP));
            ktp.tempat_lahir = cursor.getString(cursor.getColumnIndex(COLUMN_TEMAPAT_LAHIR));
            ktp.tanggal_lahir = cursor.getString(cursor.getColumnIndex(COLUMN_TANGGAL_LAHIR));
            ktp.jenis_kelamin = cursor.getString(cursor.getColumnIndex(COLUMN_JK));
            ktp.kebangsaan = cursor.getString(cursor.getColumnIndex(COLUMN_KEBANGSAAAN));
            ktp.merried = cursor.getString(cursor.getColumnIndex(COLUMN_MERRIED));
            ktp.agama = cursor.getString(cursor.getColumnIndex(COLUMN_AGAMA));
            ktp.foto_ktp = cursor.getBlob(cursor.getColumnIndex(COLUMN_FKTP));
        }
        cursor.close();
        return ktp;
    }

    public ArrayList<HashMap<String, String>> getWilayah(){
        ArrayList<HashMap<String,String>> wilayahList;
        wilayahList = new ArrayList<HashMap<String, String>>();
        String selectWilayahQuery = "SELECT * FROM " + TABLE_WILAYAH;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectWilayahQuery, null);

        if (cursor.moveToFirst()){
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(COLUMN_KODE, cursor.getString(0));
                map.put(COLUMN_PROVINSI, cursor.getString(1));
                map.put(COLUMN_KABUPATEN, cursor.getString(2));
                map.put(COLUMN_KECAMATAN, cursor.getString(3));
                wilayahList.add(map);
            } while (cursor.moveToNext());
        }

        Log.e("select sqlite ", "" + wilayahList);
        db.close();
        return wilayahList;
    }

    // menampilkan semua data dari tabel wilayahaTerpilih
    public WilayahModel getDataWil(){
        WilayahModel wil = new WilayahModel();
        database = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PILIHWILAYAH;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0){
            cursor.moveToFirst();

            wil.kdwilayah = cursor.getString(cursor.getColumnIndex(COLUMN_KD));
            wil.label = cursor.getString(cursor.getColumnIndex(COLUMN_LABEL));
        }
        cursor.close();
        return wil;
    }

    public Cursor cariWilayah(String provinsi) {
        //Open connection to read only
        String [] column = { COLUMN_KODE, COLUMN_PROVINSI, COLUMN_KABUPATEN, COLUMN_KECAMATAN};
        Cursor cursor = null;

        if (provinsi != null && provinsi.length() > 0) {
            String sql = "SELECT * FROM " + TABLE_WILAYAH + " WHERE " + COLUMN_PROVINSI + "= '" + provinsi + "'" ;
            cursor = database.rawQuery(sql, null);
            return cursor;
        }

        cursor = database.query(COLUMN_PROVINSI, column, null,null,null,null,null);
        return cursor;
    }


    // Insert data ke tabel ProfilDAO
    public void insertProfil(ProfilModel profil) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_NAMA, profil.getNama());
        values.put(COLUMN_EMAIL, profil.getEmail());
        values.put(COLUMN_HANDPHONE, profil.getHandphone());
        values.put(COLUMN_ALAMAT, profil.getAlamat());
        values.put(COLUMN_KDWIL, profil.getKodeWilayah());
        values.put(COLUMN_WILAYAH, profil.getWilayah());
        values.put(COLUMN_FDOMISILI, profil.getFoto_domisili());

        database.insert(TABLE_PROFIL, null, values);
        database.close();
    }

    // Insert data ke tabel ktpDAO
    public void insertIdentitas(IdentitasModel identitasModel) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_NIK, identitasModel.getNik());
        values.put(COLUMN_ALAMATKTP, identitasModel.getAlamatKtp());
        values.put(COLUMN_TEMAPAT_LAHIR, identitasModel.getTempat_lahir());
        values.put(COLUMN_TANGGAL_LAHIR, identitasModel.getTanggal_lahir());
        values.put(COLUMN_JK, identitasModel.getJenis_kelamin());
        values.put(COLUMN_KEBANGSAAAN, identitasModel.getKebangsaan());
        values.put(COLUMN_MERRIED, identitasModel.getMerried());
        values.put(COLUMN_AGAMA, identitasModel.getAgama());
        values.put(COLUMN_FKTP, identitasModel.getFoto_ktp());

        database.insert(TABLE_IDENTITAS, null, values);
        database.close();
    }

    // Insert data ke tabel wilyah terpilih
    public void insertWilayah(WilayahModel wilayahModel) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_KD, wilayahModel.getKdwilayah());
        values.put(COLUMN_LABEL, wilayahModel.getLabel());

        database.insert(TABLE_PILIHWILAYAH, null, values);
        database.close();
    }


//    public void update(int idpersonal, String nama, String nik, String tempat_lahir, String tanggal_lahir) {
//        SQLiteDatabase database = this.getWritableDatabase();
//
//        String updateQuery = "UPDATE " + TABLE_NAME + " SET "
//                + COLUMN_NAMA + "='" + nama + "', "
//                + COLUMN_NIK + "='" + nik + "', "
//                + COLUMN_TEMPAT_LAHIR + "='" + tempat_lahir +"',"
//                + COLUMN_TANGGAL_LAHIR + "='" + tanggal_lahir +"'"
//                + " WHERE " + COLUMN_ID + "=" + "'" + idpersonal + "'";
//        Log.e("update biodata ", updateQuery);
//        database.execSQL(updateQuery);
//        database.close();
//    }
//
//    public void delete(int idpersonal) {
//        SQLiteDatabase database = this.getWritableDatabase();
//
//        String updateQuery = "DELETE FROM " + TABLE_NAME + " WHERE " + COLUMN_ID + "=" + "'" + idpersonal + "'";
//        Log.e("update biodata ", updateQuery);
//        database.execSQL(updateQuery);
//        database.close();
//    }
}

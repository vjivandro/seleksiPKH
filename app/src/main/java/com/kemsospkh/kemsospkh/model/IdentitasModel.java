package com.kemsospkh.kemsospkh.model;

/**
 * Created by ivandro on 3/28/18.
 */

public class IdentitasModel {
    public String nik;
    public String alamatKtp;
    public String tempat_lahir;
    public String tanggal_lahir;
    public String jenis_kelamin;
    public String kebangsaan;
    public String merried;
    public String agama;
    public byte[] foto_ktp;

    public IdentitasModel(String nik, String alamatKtp,String tempat_lahir, String tanggal_lahir, String jenis_kelamin, String kebangsaan, String merried, String agama, byte[] foto_ktp) {
        this.nik = nik;
        this.alamatKtp = alamatKtp;
        this.tempat_lahir = tempat_lahir;
        this.tanggal_lahir = tanggal_lahir;
        this.jenis_kelamin = jenis_kelamin;
        this.kebangsaan = kebangsaan;
        this.merried = merried;
        this.agama = agama;
        this.foto_ktp = foto_ktp;
    }

    public IdentitasModel() {

    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getAlamatKtp() {
        return alamatKtp;
    }

    public void setAlamatKtp(String alamatKtp) {
        this.alamatKtp = alamatKtp;
    }

    public String getTempat_lahir() {
        return tempat_lahir;
    }

    public void setTempat_lahir(String tempat_lahir) {
        this.tempat_lahir = tempat_lahir;
    }

    public String getTanggal_lahir() {
        return tanggal_lahir;
    }

    public void setTanggal_lahir(String tanggal_lahir) {
        this.tanggal_lahir = tanggal_lahir;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public String getKebangsaan() {
        return kebangsaan;
    }

    public void setKebangsaan(String kebangsaan) {
        this.kebangsaan = kebangsaan;
    }

    public String getMerried() {
        return merried;
    }

    public void setMerried(String merried) {
        this.merried = merried;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public byte[] getFoto_ktp() {
        return foto_ktp;
    }

    public void setFoto_ktp(byte[] foto_ktp) {
        this.foto_ktp = foto_ktp;
    }
}

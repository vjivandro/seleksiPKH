package com.kemsospkh.kemsospkh.model;

/**
 * Created by ivandro on 3/16/18.
 */

public class Posisi {
    private String id;
    private String posisi;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPosisi() {
        return posisi;
    }

    public void setPosisi(String posisi) {
        this.posisi = posisi;
    }
}

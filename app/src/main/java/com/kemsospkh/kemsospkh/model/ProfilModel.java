package com.kemsospkh.kemsospkh.model;

import android.graphics.Bitmap;

/**
 * Created by ivandro on 3/25/18.
 */

public class ProfilModel {
    public Bitmap bmp;
    public String idpersonal;
    public String nama;
    public String email;
    public String handphone;
    public String alamat;
    public String kodeWilayah;
    public String wilayah;
    public String provinsi;
    public String kabupaten;
    public String kecamatan;
    public byte[] foto_domisili;

    public ProfilModel(String nama, String email, String handphone, String alamat, String kodeWilayah,String wilayah, byte[] foto_domisili) {
        this.nama = nama;
        this.email = email;
        this.handphone = handphone;
        this.alamat = alamat;
        this.kodeWilayah = kodeWilayah;
        this.wilayah = wilayah;
        this.foto_domisili = foto_domisili;
    }

    public ProfilModel() {

    }

    public Bitmap getBmp() {
        return bmp;
    }

    public void setBmp(Bitmap bmp) {
        this.bmp = bmp;
    }

    public String getIdpersonal() {
        return idpersonal;
    }

    public void setIdpersonal(String idpersonal) {
        this.idpersonal = idpersonal;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHandphone() {
        return handphone;
    }

    public void setHandphone(String handphone) {
        this.handphone = handphone;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKodeWilayah() {
        return kodeWilayah;
    }

    public void setKodeWilayah(String kodeWilayah) {
        this.kodeWilayah = kodeWilayah;
    }

    public String getWilayah() {
        return wilayah;
    }

    public void setWilayah(String wilayah) {
        this.wilayah = wilayah;
    }

    public byte[] getFoto_domisili() {
        return foto_domisili;
    }

    public void setFoto_domisili(byte[] foto_domisili) {
        this.foto_domisili = foto_domisili;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }
}

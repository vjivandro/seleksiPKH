package com.kemsospkh.kemsospkh.model;

import java.security.PublicKey;

/**
 * Created by ivandro on 3/29/18.
 */

public class WilayahModel {
    public String kdwilayah;
    public String provinsi;
    public String kabupaten;
    public String kecamatan;
    public String label;

    public WilayahModel() {
    }

    public WilayahModel(String kdwilayah, String provinsi, String kabupaten, String kecamatan) {
        this.kdwilayah = kdwilayah;
        this.provinsi = provinsi;
        this.kabupaten = kabupaten;
        this.kecamatan = kecamatan;
    }

    public WilayahModel(String kdwilayah, String label) {
        this.kdwilayah = kdwilayah;
        this.label = label;
    }

    public String getKdwilayah() {
        return kdwilayah;
    }

    public void setKdwilayah(String kdwilayah) {
        this.kdwilayah = kdwilayah;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
